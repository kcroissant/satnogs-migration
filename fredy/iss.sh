#!/bin/bash

for V in {01..10}
do
    echo $V
    DATE=`date +"%Y-%m-%dT%T.%NZ"`
    #curl --data "noradID=25544&source=fredy&timestamp=${DATE}&frame=60A060A0A46E609C8262A6A640E082A0A4A682A86103F02776261C6C201C53495D41524953532D49535320504D323D0D&locator=longLat&longitude=23.50000E&latitude=53.25000N" https://db-dev.satnogs.org/api/telemetry/
    curl --data "noradID=25544&source=fredy&timestamp=${DATE}&frame=60A060A0A46E609C8262A6A640E082A0A4A682A86103F02776261C6C201C53495D41524953532D49535320504D323D0D&locator=longLat&longitude=23.50000E&latitude=53.25000N" http://localhost:8010/api/telemetry/
done
