#AUTHOR: Kevin Croissant
#Pull data from a Django 4.0 instance, then send it to a newer Django instance via zmq so it can be reimported.
from datetime import datetime, timezone, timedelta
import time
import zmq
from tqdm import tqdm
import sys
from django.core import serializers
import os
import django
import json

#setup satnogs-network
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'satnogs-network.settings')
django.setup()
from network.base import models #import satnogs-network models

#Get a list of models
modelList = []
print("Here's all the models I found on this app:")
for model in django.apps.apps.get_models():
    modelList.append({'name':model.__name__, 'model':model})
    print(model.__name__)

#Sort the modelList according to the order in which we want to import
order = ['ContentType',
        'User', 
        'AntennaType',
        'Station',
        'Antenna',
        'FrequencyRange',
        'Permission',
        'Group',
        'Session',
        'Site',
        'LogEntry',
        'Token',
        'TokenProxy',
        'EmailAddress',
        'EmailConfirmation',
        'SocialApp',
        'SocialAccount',
        'SocialToken',
        'StationStatusLog',
        'Satellite',
        'Observation',
        'DemodData']
modelList = sorted(modelList, key=lambda x: order.index(list(x.values())[0]))

#######################################
#Send data to the new satnogs-network!#
#######################################
#### ZeroMQ Lazy Pirate pattern
#https://zguide.zeromq.org/docs/chapter4/#Client-Side-Reliability-Lazy-Pirate-Pattern
#migrate_from is client |  migrate_to is server.

REQUEST_TIMEOUT = 10000   #Milliseconds
SERVER_ERROR_SLEEP = 100 #Milliseconds
SERVER_ENDPOINT = "tcp://10.0.2.2:12345"

context = zmq.Context()

print("Connecting to migrate_to server")
client = context.socket(zmq.REQ)
client.connect(SERVER_ENDPOINT)

#how many objects do we want to handle at once?
#this needs to be tuned for performance and RAM consumption
batchSize = 1000

#pick a start and stop observation ID to migrate. This only applies to Observation and DemodData models
observation_start_id = 0
observation_stop_id = 9000000
t0 = time.time()
#Now we'll iterate through models, grabbing N chunks at a time, serializing, and sending them via ZMQ
for modelDict in modelList:
    model = modelDict['model']
    modelName = modelDict['name']
    print('-----------------------------------')
    print(f'Migrating model {modelName}')

    #Reset loop for each model
    idx = 0
    batchStart = 0
    batchStop = 0
    numObjects = model.objects.all().count()
    if modelName not in ['Observation', 'DemodData']:
        queryset = model.objects.filter() #no filter, grab it all at once
    for idx in tqdm(range(0, numObjects, batchSize)):
        #Grab a batch of data:
        batchStart = max([observation_start_id, idx])
        batchStop = min([observation_stop_id, idx+batchSize])
        if modelName == 'Observation':
            batch = model.objects.filter(id__gt=batchStart,id__lt=batchStop).order_by('id')
        elif modelName == 'DemodData':
            batch = model.objects.filter(observation_id__gt=batchStart,observation_id__lt=batchStop).order_by('id')            
        else:
            batch = queryset[idx:idx + batchSize]
        #Break out of the while-loop if we're done
        if not batch:
            break
        #else, process the batch!
        #Use Django's internal serializer to convert these objects to JSON:
        batchJson = json.dumps(serializers.serialize('json', batch)).encode()
        #now send the json to the new Django instance
        print(f"[{modelName}] - Sending msg {idx}")
        client.send(batchJson)
        
        retry = 0
        while True:
            if (client.poll(REQUEST_TIMEOUT) & zmq.POLLIN) != 0:
                reply = client.recv().decode()
                if reply == 'ok':
                    break
                else:
                    print('Server had some kind of issue!')
                    retry += 1
                    time.sleep(SERVER_ERROR_SLEEP)

            print(f"WARNING: No response from server after {REQUEST_TIMEOUT/1000} seconds!")
            # Socket is confused. Close and remove it.
            client.setsockopt(zmq.LINGER, 0)
            client.close()

            print("Reconnecting to server…")
            # Create new connection
            client = context.socket(zmq.REQ)
            client.connect(SERVER_ENDPOINT)
            print(f"Resending msg {idx}, retry #{retry}")
            retry += 1
            client.send(batchJson)
    #If we're done with the model, break out and allow the for-loop to continue to the next.
print('Done transferring data!')
print(time.time()-t0)
