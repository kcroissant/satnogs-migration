import time
import zmq
from tqdm import tqdm
import sys
from random import randint
from django.core import serializers
import json
import os
import django

#setup satnogs-network
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'satnogs-network.settings')
django.setup()
from network.base import models #import satnogs-network models

#### ZeroMQ Lazy Pirate pattern
#https://zguide.zeromq.org/docs/chapter4/#Client-Side-Reliability-Lazy-Pirate-Pattern
#migrate_from is client |  migrate_to is server.


## Server
context = zmq.Context()
server = context.socket(zmq.REP)
server.bind("tcp://*:12345")

okReply = 'ok'.encode()
idx = 0
while True:
    data = json.loads(server.recv().decode())

    #Deserialize using django
    modelData = serializers.deserialize("json", data, using='default')
    #Insert into Postgres
    for n in tqdm(modelData):
        n.save(using='default')
    print(f'Saved modelData #{idx} to Postgres')
    #Send an acknowledgement
    server.send(okReply) #send 0 if fail
    idx += 1
